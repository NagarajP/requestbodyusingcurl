package com.myzee.requestbody;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@RequestMapping("/myzee")
public class RequestbodyusingcurlApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequestbodyusingcurlApplication.class, args);
	}

	@PostMapping("/call1")
	public ResponseEntity<HttpStatus> call(@RequestBody Login login) {
//	public Login call(@RequestBody Login login) {
		System.out.println(login.getUsername());
		System.out.println(login.getPass());
		return ResponseEntity.ok(HttpStatus.OK);
//		return login;
	}
	
}
